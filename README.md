# QtWebApp HTTP Server in C++

This is a copy of Stefan Frings [QtWebApp](http://stefanfrings.de/qtwebapp/index-en.html) without any modification.

QtWepApp is a HTTP server library in C++, inspired by Java Servlets. For Linux, Windows, Mac OS and many other operating systems that the [Qt Framework](http://qt.io/) supports.

QtWebApp contains the following components:

* HTTP(S) Server
* Template Engine
* File Logger

These components can be used independently of each other. A very small usage example:

```C++
// The request handler receives and responds HTTP requests
void MyRequestHandler::service(HttpRequest& request, HttpResponse& response)
{
    // Get a request parameters
    QByteArray username = request.getParameter("username");

    // Set a response header
    response.setHeader("Content-Type", "text/html; charset=UTF-8");

    // Generate the HTML document
    response.write("<html><body>");
    response.write("Hello ");
    response.write(username);
    response.write("</body></html>");
}

// The main program starts the HTTP server
int main(int argc, char *argv[])
{
    QCoreApplication app(argc,argv);

    new HttpListener(
        new QSettings("configfile.ini",QSettings::IniFormat,&app),
        new MyRequestHandler(&app),
        &app);

    return app.exec();
}
```

The small memory requirement of about 2MB qualifies the web server to be used in embedded systems. For example for the [Beer brewing machine](http://sebastian-duell.de/en/mashberry/index.html) of Sebastian Düll. But it's also powerful enough for larger web services.

The logger improves disk space and performance by retaining debug messages in memory until an error occurs. No debug messages are written as long everything works fine. Changes to the configuration of the logger become active automatically without program restart.

[Source code](http://stefanfrings.de/qtwebapp/QtWebApp.zip), [Tutorial](http://stefanfrings.de/qtwebapp/tutorial/index.html), [API documentation](http://stefanfrings.de/qtwebapp/api/index.html) .

The library runs with Qt version 4.7 to 6.x. In case of Qt 6 you need to install the Qt5Compat library. It contains support for a lot of 8 Bit character encodings, that Qt6 does not support anymore by default. You may use the software under the conditions of the [LGPL License](http://www.gnu.org/licenses/lgpl.html).
